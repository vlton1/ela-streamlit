import numpy as np
from PIL import ImageChops
import PIL.Image
from PIL.ExifTags import TAGS
import requests
import streamlit as st
from streamlit_image_comparison import image_comparison
hide_streamlit_style = """
<style>
#MainMenu {visibility: hidden;}
footer {visibility: hidden;}
header {visibility: hidden;}
</style>

"""
st.markdown(hide_streamlit_style, unsafe_allow_html=True)


def ELA(image_url):
    ###Signature check###
    # Load image
    img = PIL.Image.open(requests.get(image_url, stream=True).raw)
    # Check exif info
    info = img._getexif()
    # Check software signature
    signature = False
    if info:
        for (tag, value) in info.items():
            if "Software" == TAGS.get(tag, tag):
                text = "Found Software Signature : " + value
                signature = True

    if not signature:
        text = "No Software Signature Found in the image"

    ###ELA check###
    TEMP = 'temp.jpg'
    SCALE = 10
    original = img
    original.save(TEMP, quality=75)
    temporary = PIL.Image.open(TEMP)
    diff = ImageChops.difference(original, temporary)
    d = diff.load()
    WIDTH, HEIGHT = diff.size
    for x in range(WIDTH):
        for y in range(HEIGHT):
            d[x, y] = tuple(k * SCALE for k in d[x, y])

    newsize = (800, 500)
    original = original.resize(newsize)
    diff = diff.resize(newsize)

    # text: The result of signature check
    # original: image of the input url
    # diff: image of ELA
    return (text, original, diff)


URL = st.text_input('Enter the image URL and press Enter')
if URL != '':
    try:

        with st.spinner("Analyzing...Please Wait"):
            output = ELA(URL)
        st.success('Analysis completed!')
        st.markdown('---')
        st.markdown('**Signature check result:**')
        st.markdown(output[0])
        st.markdown('---')
        image_comparison(
            img1=output[1],
            img2=output[2],
            label1="Original",
            label2="ELA",
        )
    except:
        st.error("Can't find the image, please check your URL")
        st.stop()
    #output = ELA( 'https://exifdata.com/images/1663161116.7295_shadow3.jpg')
else:
    st.header('Example')
    st.markdown('**--- Instruction Guide  ---**')
    output = ELA(
        'https://iplab.dmi.unict.it/imagej/Plugins/Forensics/ErrorLevelAnalysis/ErrorLevelAnalysis/images/lenna2.jpg')
    st.markdown('**--- Here is the Exif data(Signature check) ---**')
    st.markdown(
        '**--- Indicate whether the image is edited by some software or not ---**')
    st.text(output[0])
    st.markdown('---')
    st.markdown('**--- Here is the ELA image ---**')
    st.markdown(
        '**--- ELA shows the amount of difference that occurs during a JPEG resave. ---**')
    st.markdown(
        '**--- More white means more change, and black indicates no change. ---**')
    image_comparison(
        img1=output[1],
        img2=output[2],
        label1="Original",
        label2="ELA",
    )
